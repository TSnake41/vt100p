/*
  Copyright (C) 2019 Teddy ASTIE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#define DEFINE_CONSOLEV2_PROPERTIES

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#define ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x4

int main(int argc, char **argv)
{
  HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
  if (hOut == INVALID_HANDLE_VALUE)
    return 3;
  
  DWORD mode;
  
  if (!GetConsoleMode(hOut, &mode))
    return 3;

  if (argc > 1) {
    if (argv[1][0] == 'i') {
      printf("VT100 flag : %s", (mode & ENABLE_VIRTUAL_TERMINAL_PROCESSING) ? "on" : "off");
      return 0;
    }
    
    int enable = strtoul(argv[1], NULL, 0);
    if (enable)
      mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
    else
      mode &= ~ENABLE_VIRTUAL_TERMINAL_PROCESSING;
    
    if (!SetConsoleMode(hOut, mode))
      return 2;
  } else
    return (mode & ENABLE_VIRTUAL_TERMINAL_PROCESSING) ? 1 : 0;
  
  return 0;
}