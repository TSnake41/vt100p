# vt100p

VT100 "patcher" tool for Windows 10.
Edit the ENABLE_VIRTUAL_TERMINAL_PROCESSING flag with command-line.

## Usage

Set the ENABLE_VIRTUAL_TERMINAL_PROCESSING flag.
```vt100p [0/1]```

Check the ENABLE_VIRTUAL_TERMINAL_PROCESSING.
```vt100p i```

```vt100p```
Error codes :
 - 0 : Disabled
 - 1 : Enabled
 - 2 : Not supported/SetConsoleMode error
 - 3 : Not console

## Why ?

Some programs such as Dos9 does not support the Virtual Terminal Processing. The objective of this tool is to enable (or disable) this feature without having to patch Dos9 itself (which is possible in the Dos9 case, but in some other cases, it can be nearly impossible but very straightforward to do externally).
